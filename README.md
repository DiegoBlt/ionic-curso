Contenido del Curso:
DIA 1
1.-      Introducción al framework IONIC
2.-      Instalación del ambiente de desarrollo en Windows / linux
3.-      Crear la primera aplicación móvil
4.-      Estructura de un proyecto
5.-      Ejecución del programa
6.-      Probar la aplicación en un dispositivo móvil (físico y emulador)
7.-      Utilización de IONIC DevApp
8.-      Typescript
9.-      Angular 8
a.-      Enlaces de datos
b.-      Interpolación
c.-       Property binding
d.-      Event binding (manejar eventos)
e.-      Two way binding (vincular datos en los dos sentidos)
f.-        ngModel
g.-       Operador pipe

DIA 2
10.-   UI Components
a.-      ACTION SHEET
b.-      ALERT
c.-       BADGE
d.-      BUTTON
e.-      CARD
f.-      CHECKBOX
g.-      CHIP
h.-      DATE & TIME PICKERS
i.-        FLOATING ACTION BUTTON
j.-        ICONS
k.-       INPUT
l.-        ITEM
m.-    LIST
n.-      MEDIA
o.-      MENU
p.-      MODAL
q.-      PROGRESS INDICATORS
r.-       RADIO
s.-       RANGE
t.-       TABS
u.-      TOAST
11.-   ROUTING

DIA 3
12.-   Conexión a servicios web, para acceder a bases de datos ( Mysql, Postgres, Oracle, etc.)
a.-     CRUD a una base de datos
13.-   Maquetación y estilos
a.-      Css y sass
b.-     Grid

14.-   PLUGINS E INTERACCIÓN NATIVA
a.-      Acceso al cámara del teléfono
b.-      Lectura de datos GPS
c.-       Realizar llamadas telefónicas desde la aplicación
d.-      Acceso a la SQLLITE
15.-   Firmar de la aplicación móvil y publicación
